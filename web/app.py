import os
from flask import Flask, request, render_template, abort

app = Flask(__name__)

@app.route("/<path:path>")
def serve(path):
	if is_forbidden(path):
		abort(403)

	elif not (os.path.exists(os.path.join("./templates", path))):
		abort(404)
	return render_template(path), 200

def is_forbidden(path):
	page = path.rsplit('/',1)[-1]
	print("page:" + page)
	# look for the rightmost slash (if there is one) and see if there's a slash to the left of it
	if path.rfind('/') > 0 and path[path.rfind('/') - 1] == '/':
		print("// in request, forbidden")
		return True

	elif not(page[-5:] == '.html' or page[-4:] == '.css'):
		print("Requested file of forbidden type")
		return True

	elif page[0] == '~' or page[:2] == "..":
		print("Forbidden characters in request")
		return True

@app.errorhandler(403)
def forbidden(error):
	return render_template('403.html'), 403

@app.errorhandler(404)
def not_found(error):
	return render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
