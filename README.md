A "getting started" manual for Dockers. CIS 322, Introduction to Software Engineering, at the University of Oregon. Reference: https://docs.docker.com/engine/reference/builder/

Maintained by Patrick Higgins (phiggins@cs.uoregon.edu)

## Instructions ##
- clone this repo to your local machine
- add your ```credentials.ini``` file
- build and run with docker
